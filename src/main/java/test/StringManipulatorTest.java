package main.java.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

import main.java.StringManipulator;

public class StringManipulatorTest {

	private static final String Expected = "BNASKMDFBIRHWIEUTGOUJHK";
	
	StringManipulator stringManipulator = new StringManipulator();

	@Test
	public void cleanStringTest(){
		String result = stringManipulator.cleanString("bnaskmdfbi34739rhwieu $%^(*&TGOUJHK");
		assertEquals(result, Expected);
	}

	@Test
	public void hasUniqueCharsFalse(){
		boolean result = stringManipulator.hasUniqueChars("a2p�}}pl66e");
		assertFalse(result);
	}
	
	@Test
	public void hasUniqueCharsTrue(){
		boolean result = stringManipulator.hasUniqueChars("co%m$Puter");
		assertTrue(result);
	}
	
	@Test
	public void getWeightTest(){
		double actual = stringManipulator.getWeight("Ab5-:C");
		assertNotNull(actual);
	}
	
	@Test
	public void sortStrings() {
		String[] words = {"boy","cat","apple"};
		List actual= stringManipulator.sortStrings(words);
		
	}
	
}