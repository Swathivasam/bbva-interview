package main.java;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class StringManipulator {

	public static void main(String[] args) {
		
		//specifying CSV File path
		String csvFile = "src\\atdd\\challenge_tests.csv";
		
		//Defining Element
		BufferedReader br = null;
		String line = "";
		List<String> stringFromCSV = new ArrayList<>();
		List<String> cleanStrings = new ArrayList<>();
		Map<String, Boolean> hasUniqueChars = new HashMap<>();
		Map<String, Double> weightedString = new HashMap<>();

		try {
			
			//Using bufferReader to read the csv file
			br = new BufferedReader(new FileReader(csvFile));
			
			//Reading the header of the csv column
			String header = br.readLine();
			
			//looping through the values in the csv and populating the stringFromCSV List
			while ((line = br.readLine()) != null) {
				stringFromCSV.add(line);
			}

			//to populate cleanString List with Cleaned strings
			for (int j = 0; j < stringFromCSV.size(); j++) {
				cleanStrings.add(cleanString((String) stringFromCSV.get(j)));
			}

			//to populate the hasUniqueChars map to the strings passed from the csv
			for (int y = 0; y < cleanStrings.size(); y++) {
				hasUniqueChars.put(cleanStrings.get(y), hasUniqueChars(cleanStrings.get(y)));
			}

			//to populate the weight map to the strings passed from the csv.
			for (int z = 0; z < cleanStrings.size(); z++) {
				weightedString.put(cleanStrings.get(z), getWeight(cleanStrings.get(z)));
			}

			//to populate the sorted map for the strings passed from the csv
			String[] clenStringArray = new String[cleanStrings.size()];
			List sortedStrings = sortStrings(cleanStrings.toArray(clenStringArray));

			//Create a output file by using the printWriter 
			PrintWriter pw = new PrintWriter(new File("src\\atdd\\challenge_sorted.csv"));
			StringBuilder sb = new StringBuilder();
			sb.append("Words");
			sb.append(',');
			sb.append("Unique");
			sb.append(",");
			sb.append("Weight");
			sb.append('\n');

			for (int b = 0; b < sortedStrings.size(); b++) {
				sb.append(sortedStrings.get(b));
				sb.append(",");
				sb.append(hasUniqueChars.get(sortedStrings.get(b)));
				sb.append(",");
				sb.append(weightedString.get(sortedStrings.get(b)));
				sb.append('\n');
			}

			pw.write(sb.toString());
			pw.close();
			System.out.println("success!");

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}

	/**
	 * 
	 * This is the method to clean up the strings.
	 * 
	 * @param str
	 * @return
	 */
	public static String cleanString(String str) {
		return (str.replaceAll("[^a-zA-Z]", "").toUpperCase());
	}

	/**
	 *
	 *This method to find the Unique values in a given String. 
	 * 
	 * @param word
	 * @return
	 */
	public static boolean hasUniqueChars(String word) {
		char[] arr = word.toCharArray();
		List<Character> list = new ArrayList<>();
		for (char value : arr) {
			if (Character.isAlphabetic(value) && list.contains(value)) {
				return false;
			} else {
				list.add(value);
			}
		}
		return true;
	}

	/**
	 * 
	 * This method is used to get the weight of the given string.
	 * 
	 * @param word
	 * @return
	 */
	public static double getWeight(String word) {
		char[] arr = cleanString(word).toCharArray();
		List<Double> list = new ArrayList<>();
		double sum = 0;
		for (char value : arr) {
			list.add((double) value);
		}
		for (int i = 0; i < list.size(); i++) {
			sum += list.get(i);
		}
		return sum / list.size();
	}

	
	/**
	 * 
	 * This method is used to find the sorting of the elements passed based on the weights.
	 * 
	 * @param words
	 * @return
	 */
	public static List<String> sortStrings(String[] words) {
		HashMap<String, Double> weightedMap = new HashMap<>();
		for (int c = 0; c < words.length; c++) {
			weightedMap.put(words[c], getWeight(words[c]));
		}
		List<Entry<?, Double>> sortedList = sortHashMapByDoubleValue(weightedMap);
		List<String> list = new ArrayList<>();

		for (int d = 0; d < sortedList.size(); d++) {
			list.add((String) sortedList.get(d).getKey());
		}

		return list;
	}

	static final List<Entry<?, Double>> sortHashMapByDoubleValue(HashMap temp) {
		Set<Entry<?, Double>> entryOfMap = temp.entrySet();
		List<Entry<?, Double>> entries = new ArrayList<Entry<?, Double>>(entryOfMap);
		Collections.sort(entries, DOUBLE_VALUE_COMPARATOR);
		return entries;
	}

	static final Comparator<Entry<?, Double>> DOUBLE_VALUE_COMPARATOR = new Comparator<Entry<?, Double>>() {
		@Override
		public int compare(Entry<?, Double> o1, Entry<?, Double> o2) {
			return o1.getValue().compareTo(o2.getValue());
		}
	};

}
